object Project2Warmups {

  def hitchhiker(aList : List[Int]) : Boolean =
  {
    aList match {
      case Nil => false
      case head :: rest =>
      {
        if (head == 42) true
        else hitchhiker(rest)
      }
    }
  }

  def everyother(aList : List[Int]) : List[Int] =
  {
    aList match {
      case Nil => Nil
      case discard :: rest =>
      {
        if (rest == Nil) Nil
        else rest.head :: everyother(rest.tail)
      }
    }
  }

  def goldbach(i : Int) =
  {
    if (i <= 2) println("Error: Integer argument for the Goldbach conjecture must be >2.")
    else if (i % 2 == 1) println("Error: Integer argument for the Goldbach conjecture must be even.")
    else
    {
      var aList = goldbachHelper(2, i - 2)
      aList match {
        case Nil => println("Amazingly, you found a counterexample to the Goldbach conjecture! You are rich! Maybe!")
        case _ => println("The prime numbers " + aList(0) + " + " + aList(1) + " = " + i)
      }
    }
  }

  def goldbachHelper(j : Int, k : Int) : List[Int] =
  {
    if (j > k) Nil
    else isPrime(j) && isPrime(k) match {
      case true => List(j, k)
      case false =>
      {
        if (j == 2) goldbachHelper(j + 1, k - 1)
        else goldbachHelper(j + 2, k - 2)
      }
    }
  }

  def firstDivisor(i : Int, j : Int) : Int =
  {
    if (j > i/2) 1
    else i % j match {
      case 0 => j
      case _ => {
        if (j == 2) firstDivisor(i, j + 1)
        else firstDivisor(i, j + 2)
      }
    }
  }

  def isPrime(i : Int) : Boolean =
  {
    if (i < 2) false
    else firstDivisor(i, 2) == 1
  }

}
