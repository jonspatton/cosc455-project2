#lang racket

; COSC 455 - Programming Languages: Implementation and Design
; Project 2

; NAME: Jon S. Patton

;Lists to "translate" into numerals.
(define chinese '(ling yi er san si wu liu qi ba jiu shi))
(define english '(zero one two three four five six seven eight nine ten))

;The list of numerals to map to.
(define numerals '(0 1 2 3 4 5 6 7 8 9 10))

;Some test inputs.
(define test1 '(yi nine six ba))
(define test2 '(yi josh three si))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;Main function
;translate, add, and multiply a list of
;number words, and discard rogue entries.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define (go testinput)
  
  ;intList local variable so we don't have to rebuild the list every time.
  (let ([intList (makeIntList testinput '())])
    
    (translation intList) (newline)
    (addition intList) (newline)
    (multiplication intList) (newline)
    )
  )


;;;;;;;;;
;Helpers
;;;;;;;;;

;Format the three types of output we need.
(define (translation intList)
  (display "Translation : ")
  (printIntsWithOp intList " ")
  )

(define (addition intList)
  (display "Addition: ")
  (printIntsWithOp intList " + ")
  (display " = ")
  (display (getSum intList 0))
)

(define (multiplication intList)
  (display "Multiplication ")
  (printIntsWithOp intList " * ")
  (display " = ")
  (display (getProduct intList 1))
)

;Prints a list with an operation string between them (can be an empty string or a space as well)
(define (printIntsWithOp intList op)
  (cond
    ((empty? intList) )
    ((= (length intList) 1) (display (head intList)) (printIntsWithOp (tail intList) op))
    (else (display (head intList)) (display op)  (printIntsWithOp (tail intList) op))
    )
 )

;Builds a list of integers from a list of number words (or discards them if they aren't a number word).
;Uses getnextnumeral below
(define (makeIntList inputList resultList)
    (cond
      ((empty? inputList) resultList)
      (else
       ;getnextnumeral can return void so ignore it in that case.
        (let ([n (getnextnumeral (head inputList) chinese english numerals)])
          (cond
            ((eq? n (void)) (makeIntList (tail inputList) resultList))
            (else (makeIntList (tail inputList) (cons n resultList)))
           )
         )
       )
     )
   )

;This runs through the Chinese and English lists and checks if the head is equal to of them.
;It returns void if it's not in one of the lists. (In Scala I returned -1 but this isn't much differnet
;and I guess it depends on which type of "bad" value you want to check for on return.)
(define (getnextnumeral elem chin eng num)
  (unless (empty? chin)
    (cond
      ((or (eq? (head chin) elem) (eq? (head eng) elem)) (head num))
      (else (getnextnumeral elem (tail chin) (tail eng) (tail num)))
      )
    )
  )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;Redefinitions of built-in functions, for reading comprehension
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;Mathematical operations
(define (getSum intList sum)
  ;(cond
  ;  ((empty? intList) sum)
  ;  (else (getSum (tail intList) (+ (head intList) sum)))
  ;  )
  (foldl + sum intList)
  )

(define (getProduct intList prod)
  ;(cond
  ;   ((empty? intList) prod)
  ;  (else (getProduct (tail intList) (* (head intList) prod)))
  ;  )
  (foldl * prod intList)
  )


;Redefine some operation names to something I'm more familiar with.
(define head (lambda (l) (car l)))
(define tail (lambda (l) (cdr l)))
