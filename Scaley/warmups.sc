// COSC 455 - Programming Languages: Implementation and Design
// Project 2

// NAME: Jon S. Patton

/**
  * 1. Meaning of Life (1 point). Create a function, named hitchhiker, that takes an integer list and returns a
  * Boolean indicating whether the number 42 in the list.
  * @param aList
  * @return
  */

def hitchhiker(aList : List[Int]) : Boolean =
{
  aList match {
    case Nil => false
    case head :: rest =>
    {
      if (head == 42) true
      else hitchhiker(rest)
    }
  }
}

//Test
hitchhiker(List(1, 2, 42, 5, 6))

/**
  * 2. Every Other Element (2 points). Create a function, named everyother, that takes an integer list as a
  * parameter and returns an integer list consisting of every other element of the parameter list. For
  * example, everyother ([3, 5, 7, 11, 13, 17, 19, 29, 31, 41, 43]) should return [5, 11, 17, 29, 41].
  * @param aList
  * @return
  */

def everyother(aList : List[Int]) : List[Int] =
{
  aList match {
    case Nil => Nil
    case discard :: rest =>
    {
      if (rest == Nil) Nil
      else rest.head :: everyother(rest.tail)
    }
  }
}

//Test
everyother(List(1, 2, 42, 5, 6))
everyother(List(3, 5, 7, 11, 13, 17, 19, 29, 31, 41, 43))

/**
  * 3. Goldbach’s Conjecture (2 points). Create a function, named goldbach, that takes an integer and prints the
  * solution satisfying the Goldbach Conjecture. The Goldbach Conjecture states that every positive even number
  * greater than 2 is the sum of two prim numbers. For example, 28 = 5 + 23. Your function is to find the two prime
  * numbers that sum up to a given even integer and print the composition. For example goldbach(28) would print
  * 5 + 23 = 28. You should provide error checking to make sure the integer parameter is even and greater than 2.
  * @param i
  */
def goldbach(i : Int) =
{
  if (i <= 2) println("Error: Integer argument for the Goldbach conjecture must be >2.")
  else if (i % 2 == 1) println("Error: Integer argument for the Goldbach conjecture must be even.")
  else
  {
    var aList = goldbachHelper(2, i - 2)
    aList match {
      case Nil => println("Amazingly, you found a counterexample to the Goldbach conjecture! " +
        "You are rich! Maybe!")
      case _ => println("The prime numbers " + aList(0) + " and " + aList(1) + " sum to " + i +
        ", satisfying the Goldbach conjecture.")
    }
  }
}

/**
  * Do the thing only if you're both prime
  * @param j prime candidate
  * @param k prime candidate
  * @return
  */
def goldbachHelper(j : Int, k : Int) : List[Int] =
{
  if (j > k) Nil
  else isPrime(j) && isPrime(k) match {
    case true => List(j, k)
    case false =>
    {
      if (j == 2) goldbachHelper(j + 1, k - 1)
      else goldbachHelper(j + 2, k - 2)
    }
  }
}

/**
  * Reports whether an integer is prime.
  * @param i
  * @return
  */
def isPrime(i : Int) : Boolean =
{
  if (i < 2) false
  else firstDivisor(i, 2) == 1
}

/**
  * Discovers the first integer divisor of an integer; reports 1 if the number is 1 or prime
  * (that is, no candidate divisors above i/2). Steps by 2 from numbers 3 onward. Still
  * horribly inefficient compared to using a prime sieve for several inputs, but I'm
  * forced to do this recursively.
  * @param i
  * @param j should always be set to 2 or 3 initially.
  * @return
  */
def firstDivisor(i : Int, j : Int) : Int =
{
  if (j > i/2) 1
  else i % j match {
    case 0 => j
    case _ => {
      if (j == 2) firstDivisor(i, j + 1)
      else firstDivisor(i, j + 2)
    }
  }
}

//Tests for helpers
firstDivisor(7, 2)

isPrime(4)

//Godbach tests
goldbach(2)

goldbach(5)

goldbach(12)

goldbach(128)

goldbach(120008)