// COSC 455 - Programming Languages: Implementation and Design
// Project 2

// NAME: Jon S. Patton

/**
  * Lists of number words
  */
val chinese: List[String] = List("ling", "yi", "er", "san", "si", "wu", "liu", "qi", "ba", "jiu", "shi")
val english: List[String] = List("zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten")

/**
  * The list of numerals to map the number words to.
  */
val numeral: List[Int]    = List(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10)

/**
  * Main function: Just calls the helpers.
  * There's another way to do this, which is how I did it in Scheme.
  * Use the following to build a list of integers from the input from the outset.
  * Then just pass that to a function that prints out the numerals and their sum
  * or product when needed.
  *
  * That version's a little cleaner and less repetitive in the code, but it also might
  * be less in keeping with the spirit of a program that translates or adds a list of
  * words. The only reason it's more efficient is because we're only calling "Go."
  *
  * If instead you wanted a way to call "doAddition(list of strings)" then you would need
  * this version of the program.
  */

def go(lst : List[String]) =
{
  println("Translation: " + doTranslation(lst))
  println("Addition: " + doAddition(lst, 0))
  println("Multiplication: " + doMultiplication(lst, 1))
}

/*
Helpers
 */
/**
  * Translate a list of strings into numerals printed out to the console.
  * @param list
  * @return
  */
def doTranslation(list: List[String]): String =
{
  list match
  {
    case Nil => ""
    case _ => {
      var n = numberify(list.head)
      n >= 0 match {
        case false => doTranslation(list.tail)
        case _ => n + " " + doTranslation(list.tail)
      }
    }
  }
}

/**
  * Print out numbers, then their sum.
  * @param summands
  * @param sum
  * @return
  */
def doAddition(summands: List[String], sum : Int): String =
{
  summands match
  {
    case Nil => " = " + sum
    case _ => {
      var n = numberify(summands.head)
      n >= 0 match {
        case false => doAddition(summands.tail, sum)
        case _ => n + opsign(summands.tail, " + ") + doAddition(summands.tail, sum + n)
      }
    }
  }
}

/**
  * Print out numbers, then their product.
  * @param terms
  * @param product
  * @return
  */
def doMultiplication(terms: List[String], product : Int): String =
{
  terms match
  {
    case Nil => " = " + product
    case _ => {
      var n = numberify(terms.head)
      n >= 0 match {
        case false => doMultiplication(terms.tail, product)
        case _ => n + opsign(terms.tail, " * ") + doMultiplication(terms.tail, product * n)
      }
    }
  }
}

/**
  * A little helper function to insert a + or * sign only if there are no items left in the tail.
  * Seems silly but it's nicer than an extra layer of list matching elsewhere.
  * @param l
  * @param op
  * @return
  */
def opsign(l : List[String], op: String): String =
{
  l match {
    case Nil => " "
    case _ => op
  }
}

def numberify(s: String): Int = {
  getNextNumeral(s, chinese, english, numeral)
}

def getNextNumeral(s : String, chinese : List[String], english: List[String], numeral : List[Int]) : Int = {
  english match {
    case Nil => -1
    case _ =>
    {
      if (s.equals(chinese.head) || s.equals(english.head)) numeral.head
      else getNextNumeral(s, chinese.tail, english.tail, numeral.tail)
    }
  }
}

// Test
go(List("yi", "josh", "three", "si"))
go(List("yi", "nine", "six", "ba"))

/**
  * Turn a list of strings into numerals.
  * @param ins
  * @param outs
  * @return
  */

def buildIntListFromStringList(ins : List[String], outs : List[Int]) : List[Int] =
{
  ins match {
    case Nil => outs
    case head :: tail => {
      var n = getNextNumeral(head, chinese, english, numeral)
      n >= 0 match {
        case true => buildIntListFromStringList(tail, n :: outs)
        case false => buildIntListFromStringList(tail, outs)
      }
    }
  }
}