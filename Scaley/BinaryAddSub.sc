// COSC 455 - Programming Languages: Implementation and Design
// Project 2

// NAME: Jon S. Patton

/**
  * This function does the binary addition when there are uneven lists and still must
  * // finish the add with the carry bits.
  *
  * @param remainingBits the leftovers after one of the lsts is exhausted
  * @param carryBit a bit
  * @return the exhausted list, with carry if any
  */
def finishBinaryAdd(remainingBits: List[Boolean], carryBit: Boolean): List[Boolean] =
{
  remainingBits match {
    case Nil =>
      {
        if (carryBit) carryBit :: Nil
        else Nil
      }
    case head :: tail =>
      {
        //var c = getNextCarryBit(head, false, carryBit)
        addBits(head, false, carryBit) :: finishBinaryAdd(tail, getNextCarryBit(remainingBits.head, false, carryBit))
      }
  }
}

/**
  * This function determines what the next carry bit should be based on current bits.
  * @param pBit a bit
  * @param qBit another bit
  * @param carryBit yet another bit
  * @return leftover of p + q + c
  */
def getNextCarryBit(pBit: Boolean, qBit: Boolean, carryBit: Boolean): Boolean =
{
  //Carry if any two of the three are true
  (pBit && qBit && carryBit) || (pBit && !qBit && carryBit) || (!pBit && qBit && carryBit) || (pBit && qBit && !carryBit)
}

/**
  * This function does the binary addition of two Booleans and a carry bit.
  * @param pBit a bit
  * @param qBit another bit
  * @param carryBit yet another bit
  * @return p + q + c
  */
def addBits(pBit: Boolean, qBit: Boolean, carryBit: Boolean): Boolean =
{
  //There are four ways to get a 1
  (!qBit && !pBit && carryBit) || (pBit && !qBit && !carryBit) || (!pBit && qBit && !carryBit) || (qBit && pBit && carryBit)
}

/**
  * This function does the binary addition of two boolean lists. Note that the lists may not be equal in length.
  * @param pBits summand 1
  * @param qBits summand 2
  * @param carryBit result of pi + qi = 10 or 11
  * @return p + q in boolean
  */
def doBinaryAddition(pBits: List[Boolean], qBits: List[Boolean], carryBit: Boolean): List[Boolean] =
{
  if (pBits == Nil && qBits == Nil) if (carryBit) Nil else carryBit :: Nil
  else if (pBits == Nil) if (!carryBit) qBits else finishBinaryAdd(qBits, carryBit)
  else if (qBits == Nil) if (!carryBit) pBits else finishBinaryAdd(pBits, carryBit)
  else
  {
    addBits(pBits.head, qBits.head, carryBit) :: doBinaryAddition(pBits.tail, qBits.tail, getNextCarryBit(pBits.head, qBits.head, carryBit))
  }
}

/**
  * This function converts a binary integer list into its corresponding boolean list.
  * @param intList 1s and 0s
  * @return trues and falses
  */
def convertIntListToBooleanList(intList: List[Int]) : List[Boolean] =
{
  intList match {
    case Nil => Nil
    case head :: tail => {
      head match {
        case 0 => false :: convertIntListToBooleanList(tail)
        case 1 => true :: convertIntListToBooleanList(tail)
      }
    }
  }
}

/**
  * This function converts a boolean list into its corresponding binary integer list.
  * @param booleanList A list of trues and falses
  * @return 1s and 0s
  */
def convertBooleanListToIntList(booleanList: List[Boolean]): List[Int] =
{
  booleanList match {
    case Nil => Nil
    case head :: tail => {
      head match {
        case false => 0 :: convertBooleanListToIntList(tail)
        case true => 1 :: convertBooleanListToIntList(tail)
      }
    }
  }
}

/**
  * This is the "main" function to do binary addition. This function should:
  *     1. Convert the input parameter lists from integers to boolean. DONE
  *     2. Reverse the lists (since binary addition is performed right to left). Use Scala reverse. DONE
  *     3. Perform the binary addition with the doBinaryAddition function.
  *     4. Reverse the lists (to get back in proper order). Use Scala reverse.
  *     5. Convert the answer back to binary integer form for output.
  * Note that the initial carry bit is assumed to be 0 (i.e., false).
  *
  * @param pList One of the binary integers
  * @param qList The other binary integer
  * @return The result of p + q
  */
def binaryAddition(pList: List[Int], qList: List[Int]) : List[Int] =
{
  convertBooleanListToIntList(doBinaryAddition(convertIntListToBooleanList(pList).reverse,
    convertIntListToBooleanList(qList).reverse, false))
    .reverse

}

/**
  * Take two's complement of q, add to p, and account for positive or negative values.
  * @param pList The minuend or whatever it's called
  * @param qList The subtractand or whatever it's called
  * @return The result of p - q
  */
def binarySubtraction(pList: List[Int], qList: List[Int]) : List[Int] =
{

  //The lists have to be of equal length
  if (pList.length > qList.length)
    {
      binarySubtraction(pList, 0::qList)
    }
  else if(qList.length > pList.length)
    {
      binarySubtraction(0::pList, qList)
    }
  else {

    //pList + Two's complement of qList
    var qcomplement = twosComplement(qList)

    var bin = convertBooleanListToIntList(doBinaryAddition(convertIntListToBooleanList(pList).reverse,
      qcomplement, false))
      .reverse

    bin.head match {
      case 0 => bin.tail
      case 1 => convertBooleanListToIntList(twosComplement(bin))
    }
  }
}

/**
  * Helper for subtraction
  * @param bin List of 1 and 0
  * @return a list with all values flipped +1, reversed, converted to boolean
  */
def twosComplement(bin: List[Int]) : List[Boolean] =
{
  doBinaryAddition(flipList(convertIntListToBooleanList(bin).reverse),
    List(true), false)
}

/**
  * Helper for two's complement
  * @param bin A list of Booleans
  * @return A list with all values toggled.
  */
def flipList(bin : List[Boolean]): List[Boolean] =
{
  bin match {
    case Nil => Nil
    case head :: tail => {
      head match {
        case true => false :: flipList(tail)
        case false => true :: flipList(tail)
      }
    }
  }
}

// Test Cases
              val pTest1: List[Int] = List (  1, 1, 1, 1, 0)
              val qTest1: List[Int] = List(      1, 0, 1, 1)
val test1ExectedSolution: List[Int] = List(1, 0, 1, 0, 0, 1)

              val pTest2: List[Int] = List(1, 0, 0, 1, 1, 0, 1)
              val qTest2: List[Int] = List(      1, 0, 0, 1, 0)
val test2ExectedSolution: List[Int] = List(1, 0, 1, 1, 1, 1, 1)


              val pTest3: List[Int] = List(1, 0, 0, 1, 0, 0, 1)
              val qTest3: List[Int] = List(      1, 1, 0, 0, 1)
val test3ExectedSolution: List[Int] = List(1, 1, 0, 0, 0, 1, 0)

              val pTest4: List[Int] = List(1, 0, 0, 0, 1, 1, 1)
              val qTest4: List[Int] = List(      1, 0, 1, 1, 0)
val test4ExectedSolution: List[Int] = List(1, 0, 1, 1, 1, 0, 1)

val test5ExectedSolution: List[Int] = List(1, 1, 1, 0, 1, 1)
val test6ExectedSolution: List[Int] = List(1, 1, 0, 0, 0, 1)

// Testing binary addition.
if (binaryAddition(pTest1, qTest1).equals(test1ExectedSolution)) println("Test 1 passes!") else println("Test 1 fails.")
if (binaryAddition(pTest2, qTest2).equals(test2ExectedSolution)) println("Test 2 passes!") else println("Test 2 fails.")
if (binaryAddition(pTest3, qTest3).equals(test3ExectedSolution)) println("Test 3 passes!") else println("Test 3 fails.")
if (binaryAddition(pTest4, qTest4).equals(test4ExectedSolution)) println("Test 4 passes!") else println("Test 4 fails.")

// Testing binary subtraction.
if (binarySubtraction(pTest2, qTest2).equals(test5ExectedSolution)) println("Test 5 passes!") else println("Test 5 fails.")
if (binarySubtraction(pTest4, qTest4).equals(test6ExectedSolution)) println("Test 6 passes!") else println("Test 6 fails.")
