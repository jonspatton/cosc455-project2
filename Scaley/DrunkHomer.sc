// COSC 455 - Programming Languages: Implementation and Design
// Project 2

// NAME: Jon S. Patton

import scala.collection.mutable.ArrayBuffer

/*

Strategy:
1. Make a 3D grid of triples [(x, val b1 = true), (y, val b2 = true), (z, val b3 = true)].

2. Let
x E {1 = green dress, 2 = saxophone book, 3 = slingshot, 4 = pacifier}  = gifts
y E {1 = Leftorium, 2 = Sprawl-Mart, 3 = Try-N-Save, 4 = King Toots}    = Stores
z E {1, 2, 3, 4}                                                        = Order

3. Each triple is a statement. True indicates that the node is still possible.

4. A clue is evaluated in such a way as to set some group of triples to contain false values.

5. When the clues are exhausted, the program reports the nodes that are still of the form [(x, true), (y, true), (z, true)].
If we've done everything correctly, there will be exactly four such tuples, containing the (manually determined) solution:

Order     Store       Gift
---------------------------------
1         Try-n-Save  Slingshot
2         Leftorium   Dress
3         Sprawl Mart Pacifier
4         King Toots  Book

Clues, translated into logical statements and then pseudocode:

1. He bought the saxophone book at King Toots
Book <-> King Toots

Break it up into two statements (that is, -> first then <- second), setting each axis one at a time
If x = book && y != King Toots -> set all false
If x != book && y = King Toots -> set all false

2. The store he visited immediately after buying the slingshot was not Sprawl-Mart
n = size of order (4 of course)
slingshot -> order < n
Sprawl Mart -> order < n
x = slingshot && z = n -> set all false
y = Sprawl Mart && z = n -> set all false

3. The Leftorium was his second stop
Order = 2 <-> Leftorium

Same strategy as clue 1.
If y = Leftorium && z != 2 -> set all false
If y != Leftorium && z = 2 -> set all false

4. Two stops after leaving Try-N-Save, he bought the pacifier
pacifier -> order > 2
Try-N-Save -> Order < n - 1
Pacifier -> !Try-N-Save
Try-N-Save -> !Pacifier

If x = pacifier && z <= 2 -> set all false
if y = Try-N-Save && z >= n - 1 -> set all false
If x = pacifier && y = Try-N-Save -> set all false

5. The resulting grid is not finished. There are two branches now.

Algorithm
---------
Things have changed = true (set so that we can initiate the loop)
Loop until only 4 possible or things have changed is false
  Things have changed = false (set so the loop will eventually terminate)

  Tally the instances of each gift, store, and order.
      E.g.  giftsInstances = [3, 2, 2, 1] (the actual state we're in.)
      storeInstances = [2, 1, 3, 2] OR
      orderInstances = [3, 2, 2, 1]
      (You only need one of these -- if any aspect of an item was solved, the others are as well.)
  a: Progress is impossible if have reached a point where none of the instances are 1 OR
      none of the possibilities were marked false since we last ran the loop.
  b: Otherwise,
      locate a (each) triple associated with an instance of 1. Take the other
      elements of that triple and set all other triples with those elements
      to false. E.g. If we look at giftsInstances, we find that pacifier is 1.
      Let the tuple associated with it be t = (x, y, z).
      We now have a condition that if xi != x, then it's false any time yi = y or zi = z.
        If we find any, set the "things have changed" flag to true

      Move onto the next instance of 1.

 */

val giftsMap = Map("green dress" -> 1, "saxophone book" -> 2, "slingshot" -> 3, "pacifier" -> 4)
val storesMap = Map("Leftorium" -> 1, "Sprawl-Mart" -> 2, "Try-N-Save" -> 3, "King Toots" -> 4)
val orderMap = Map("1" -> 1, "2" -> 2, "3" -> 3, "4" -> 4)
val gifts = giftsMap.keys.toList
val stores = storesMap.keys.toList
val order = orderMap.values.toList
var grid = ArrayBuffer[((String, Boolean), (String, Boolean), (Int, Boolean))]()
var n = 0
var m = gifts.length
var fin : List[(String, String, Int)]= Nil

def fillGrid() = {
  for (i <- 0 to m - 1)
    for (j <- 0 to m - 1)
      for (k <- 0 to m - 1) {
        grid.append(((gifts(i), true), (stores(j), true), (order(k), true)))
      }
  n = grid.length
}


fillGrid()

def satisfyClue(x : List[String], y : List[String], z : List[Int]) = {
  for (i <- 0 to n - 1) {
    if (x.contains((grid(i) _1) _1) && y.contains((grid(i) _2) _1) && z.contains((grid(i) _3) _1)) {
      grid(i) = grid(i).copy(((grid(i) _1) _1, false), ((grid(i) _2) _1, false), ((grid(i) _3) _1, false))
    }
  }
}

def tallyInstances(a : Map[String, Int], g: Boolean, s: Boolean, o: Boolean) : Array[Int] =
{
  var instances = new Array[Int](a.size)
  for (i <- grid) {
    if (allTrueTuple(i)) {
      var xi = ((i) _1)
      var yi = ((i) _2)
      var zi = ((i) _3)
      //Holy shit is this a stupid, stupid function name instead of "get"
      if (g) instances(a.apply((xi) _1) - 1) += 1
      else if (s) instances(a.apply((yi) _1) - 1) += 1
      else if (o) instances(a.apply(((zi) _1).toString) - 1) += 1
    }
  }
  return instances
}

def solveAfterClues(): Boolean = {
  var changed: Boolean = true
  var solved: Boolean = false
  var cutoff = 0 //Termination insurance

  while (changed && !solved) {
    //We know we're done.
    if (cutoff > 50 || solved) return solved

    changed = false

    var giftsInstances = tallyInstances(giftsMap, true, false, false)
    var storesInstances = tallyInstances(storesMap, false, true, true)
    var orderInstances = tallyInstances(orderMap, false, false, true)
    //If there are only instances of 1 left, we're necessarily done.
    solved = verifySolution(giftsInstances, storesInstances, orderInstances)

    for (i <- 0 to giftsInstances.length - 1) {
      if (solved) return solved //Early return: If the solution exists, this is it.
      else if (giftsInstances(i) == 1) {
        solverHelper(gifts.toArray, i, true, false, false)
        changed = true
      }
    }
    for (i <- 0 to storesInstances.length - 1) {
      if (solved) return solved //Early return: If the solution exists, this is it.
      else if (storesInstances(i) == 1) {
        solverHelper(stores.toArray, i, false, true, false)
        changed = true
      }
    }
    for (i <- 0 to orderInstances.length - 1) {
      if (solved) return solved //Early return: If the solution exists, this is it.
      else if (orderInstances(i) == 1) {
        solverHelper(order.toArray, i, false, false, true)
        changed = true
      }
    }
    cutoff += 1
  }
  return solved
}

def solverHelper(a : Array[Any], i : Int, g : Boolean, s : Boolean, o : Boolean): Unit = {
  var t = findSpecificTuple(a(i))
  var x: String = (((t) _1) _1)
  var y: String = (((t) _2) _1)
  var z: Int = (((t) _3) _1)
  if (g) {
    satisfyClue(gifts.filter(!_.equals(x)), stores.filter(_.equals(y)), order)
    satisfyClue(gifts.filter(!_.equals(x)), stores, order.filter(_ == z))
  }
  else if (s) {
    satisfyClue(gifts.filter(_.equals(x)), stores.filter(!_.equals(y)), order)
    satisfyClue(gifts, stores.filter(!_.equals(y)), order.filter(_ == z))
  }
  else if (o) {
    satisfyClue(gifts.filter(!_.equals(x)), stores.filter(_.equals(y)), order.filter(!_.equals(z)))
    satisfyClue(gifts.filter(_.equals(x)), stores.filter(!_.equals(y)), order.filter(!_.equals(z)))
  }
  reEval2and4(x, y, z)
}

def reEval2and4(x: String, y: String, z: Int) = {
  if (x.equals("pacifier")) {
    satisfyClue(gifts, stores.filter(!_.equals("Try-N-Save")), order.filter(_ == z - 2))
  }
  if (y.equals("Sprawl-Mart")) {
    satisfyClue(gifts.filter(_.equals("slingshot")), stores, order.filter(_ == z - 1))
  }
}


def allTrueTuple(tuple: ((String, Boolean), (String, Boolean), (Int, Boolean))) : Boolean = {
  return (((tuple) _1) _2) && (((tuple) _2) _2) && (((tuple) _3) _2)
}

//Can only be used if you're sure there's only one tuple containing s
def findSpecificTuple(s : Any) : ((String, Boolean), (String, Boolean), (Int, Boolean)) = {
  for (i <- grid) {
    if (allTrueTuple(i) && ((((i) _1) _1).equals(s)) || (((i) _2) _1).equals(s) || (((i) _3) _1).equals(s)) {
      return i
    }
  }
  return null
}

def verifySolution(giftsInstances : Array[Int], storesInstances : Array[Int],
                   orderInstances : Array[Int]): Boolean =
{
  return ((!giftsInstances.map(_ > 1).contains(true)) &&
    (!storesInstances.map(_ > 1).contains(true)) &&
    (!orderInstances.map(_ > 1).contains(true)))
}

def findAllTrue(): List[(String, String, Int)] = {
  var lst: List[(String, String, Int)] = Nil
  for (i <- grid) {
    if (allTrueTuple(i)) {
      var x: String = (((i) _1) _1)
      var y: String = (((i) _2) _1)
      var z: Int = (((i) _3) _1)
      lst = (x, y, z) :: lst
    }
  }
  return lst.sortBy(_._3).reverse
}

def printList(lst : List[(String, String, Int)]): Unit = {
  lst match {
    case Nil => println("---------------------------------")
    case _ => {
      printList(lst.tail)
      println("Homer bought the " + ((lst.head)_1) + " at the "
        + ((lst.head)_2) + " on stop number " + ((lst.head)_3))
    }
  }
}

//Clue 1
//If x = book && y != King Toots -> set all false
//If x != book && y = King Toots ->
satisfyClue(List("saxophone book"), stores.filter(!_.equals("King Toots")), order)
satisfyClue(gifts.filter(!_.equals("saxophone book")), List("King Toots"), order)

//Clue 2
//x = slingshot && z = n -> [(x, b1 = false), (y, b2 = b2), (z, b3 = b3)]
//y = Sprawl Mart && z = n -> [(x, b1 = b1), (y, b2 = false), (z, b3 = b3)]
satisfyClue(List("slingshot"), stores, order.filter(_ == m))
satisfyClue(gifts, List("Sprawl-Mart"), order.filter(_ == order.length))

//Note that clue 2 is reevaluated when we either know where the slingshot was bought
//or we know what was bought at Sprawl-Mart

//Clue 3
//If y = Leftorium && z != 2 ->
//If y != Leftorium && z = 2 ->
satisfyClue(gifts, stores.filter(_.equals("Leftorium")), order.filter(_ != 2))
satisfyClue(gifts, stores.filter(!_.equals("Leftorium")), order.filter(_ == 2))

//Clue 4
// If x = pacifier && z <= 2 ->
//if y = Try-N-Save && z >= m - 1 ->
//If x = pacifier && y = Try-N-Save ->
satisfyClue(List("pacifier"), stores, order.filter(_ <= 2))
satisfyClue(gifts, List("Try-N-Save"), order.filter(_ >= m - 1))
satisfyClue(List("pacifier"), List("Try-N-Save"), order)

//Note that clue 4 is reevaluated when we know when the pacifier was bought


//Determine (a) if the puzzle is unsolveable or (b) produce the solution
if (solveAfterClues()) printList(findAllTrue())
else println("The puzzle could not be solved by this program given the input parameters")
